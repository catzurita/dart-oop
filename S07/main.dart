import './worker.dart';

void main() {
  Doctor personA = new Doctor(firstName: 'John', lastName: 'Doe');

  Carpenter carpenter = new Carpenter();

  print(carpenter.getType());
}

abstract class Person {
  //The class person defines that a 'getFullName' must be implemented
  // However, it does not tell the concrete class How to implement it
  String getFullName(); //Example of an Abstract method

}

// Concrete class
class Doctor implements Person {
  String firstName;
  String lastName;

  Doctor({required this.firstName, required this.lastName});

  String getFullName() {
    return 'Dr. ${this.firstName} ${this.lastName}';
  }
}

class Attorney implements Person {
  String getFullName() {
    return 'Atty';
  }
}


/* 
  Abstract classes can't be instantiated.
    - makses sure the implementation of classes and methods

  In making an abtract class:
  Syntax:
  abstract class <className>{} 

  ex: in javascript same sya sa pag if(this.constructor == Person) , sa dart dinedeclare nalang siya 

  Abstract method - a method w/o a block of code 
    - no implementation of codes needed
    - making an abstract method, is same sa java script na nag thothrow ng error pag wala yung method na kelangan
    ex: if(this.getFullName == undefined)

  Concrete class - this is what implements the methods from the abstract class 
  Syntax in making a concrete class:
    class <concreteClass> implements <abastractClass>{}

  pag inherit ang isang abstract class
  instead of extends, gagamitin is implements
 */