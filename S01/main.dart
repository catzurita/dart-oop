// The keyword 'void' means the function will not return anything
// The ssyntax of a function is: <return-type> <function-name>(prameters){}
void main() {
  String firstName = 'Kobe';
  String? middleName = null;
  // Since variables can't contain a null value, for it to be able to accept a null value, we put "?" in front of the data types
  String lastName = 'Bryant';
  int age = 31; // data type for numbers without decimal points
  double height = 172.45; // data type for numbers with decimal points
  num weight = 64.32; //data type that accepts numbers w/ or w/o decimal points
  bool isRegistered = false; //data typ efor conditions, either true or false
  List<num> grades = [98.2, 87.88, 91.2];
  /* 
  List<num> grades - this implies that the list grades, has values of data type num 
  
  = we can declare a list w/o <> if we want to have a list that has different data types,
  - but for dart that is a bad practice since in making lists, we need to compose a list that has the same data types,

  accessing lists
  <ListsName>[indexNO.]
  */

  /* 
  Map person = new Map{}; Map is an object, which has data of format of key-value pair in JSON format, wherein both key and value have a single quote ''

  Map<key-type, value-type> - we can assign the data type for the key value pairs,
ex: 
 Map<String, dynamic> person = {
    'name': 'Brandon',
    'batch': '213'
    };
  <> - this where we can declare the data type, for example in a list


  data type dynamic - declares that data will have a combination of different data types 
     */
  Map<String, dynamic> personA = {'name': 'Brandon', 'batch': 213};
  // Accesing the value of a Map is using the square bracket notation, <MapName>['keyName']
  print(personA['name']);

  // Using final

  final now = DateTime.now();

  const String companyAcronym = 'FFUF';
/* 
final vs const,
  pag kinreate si final, kahit wala syang value, pero pag initialized na sya with a value, yun na yung pinaka value nya, din na pwede palitan
    - once it has been set it cannot be changed

  for const, pag kinreate sya, dapat nakainitialize na sya agad sa isang value, and yung value na yun di na din pwede palitan
 */
  // ALternative syntax for declaring object
  Map<String, dynamic> personB = new Map();
  personB['name'] = 'Juan';
  personB['batch'] = 89;

  // var is same as let or const in javascript, in declaring variable, but in dart, it has an inherent ability to determin the data type of a variable
  var name = 'John';
  var number = 1;
  print('Hello' + name); //The hello is concatenated with the name vairiable
  name = number.toString();

  print(firstName + ' ' + lastName);
  print('Full Name: $firstName $lastName');
  //same sa javascript na nagamit ng backtick, then declare function, wala lang {}
  print('Age: ' + age.toString());
  print('Height: ' + height.toString());
  print('Weight: ' + weight.toString());
  print('Registered: ' + isRegistered.toString());
  print('Grades: ' + grades.toString());
  print('Current Date and Time: ' + now.toString());
}
