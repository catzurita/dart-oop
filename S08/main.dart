// Create an s08/main.dart file and put the comments here to the Dart file.

// Create an abstract class named Equipment and an
// abstract method named describe inside the abstract class.

// Create the classes Bulldozer, TowerCrane, and Loader
// that will all implement the abstract class Equipment.

// Create objects for each of the vehicle type with the following information.
// - Bulldozer: Caterpillar D10, U blade
// - Tower Crane: 370 EC-B 12 Fibre, 78m hook radius, 12,000kg max capacity
// - Loader: Volvo L60H, wheel loader, 16530lbs tipping load

// Inside the main() method, create a List for the
// equipment (using the Equipment type) and give it an initial value of [].

// Add the created objects to the list created earlier.

// Loop through each vehicle in the list and execute the describe() method.

// Sample Output
// - The bulldozer Caterpillar D10 has a U blade.
// - The tower crane 370 EC-B 12 Fibre has a radius of 78 and a max capacity of 12000.
// - The loader Volvo L60H is a wheel loader and has a tipping load of 16530 lbs.

void main() {
  List<Equipment> equipments = [];

  Bulldozer bulldozer = new Bulldozer(
    model: 'Caterpillar D10', 
    equipmentType: 'U blade'
    );

  TowerCrane towerCrane = new TowerCrane(
    model: '370 EC-B 12 Fibre', 
    hookRadius: 78, 
    maxCapacity: 12000
    );

  Loader loader = new Loader(
    model: 'Volvo L60H', 
    equipmentType: 'wheel loader ', 
    tippingLoad: 16530
    );

  equipments.add(bulldozer);
  equipments.add(towerCrane);
  equipments.add(loader);

  equipments.forEach((equipment) {
    print(equipment.describe());
  });
}

abstract class Equipment {
  String describe();
}

// for bulldozer
class Bulldozer implements Equipment {
  String model;
  String equipmentType;

  Bulldozer({
    required this.model, 
    required this.equipmentType
    });

  String describe() {
    return 'The Bulldozer ${this.model} has a ${this.equipmentType}.';
  }
}

// For TowerCrane
class TowerCrane implements Equipment {
  String model;
  num hookRadius;
  num maxCapacity;

  TowerCrane({
    required this.model,
    required this.hookRadius,
    required this.maxCapacity
    });

  describe() {
    return 'The Tower Crane ${this.model} has a radius of ${this.hookRadius.toString()} and a max capacity of ${this.maxCapacity.toString()}.';
  }
}

// Class for loader
class Loader implements Equipment {
  String model;
  String equipmentType;
  num tippingLoad;

  Loader({
    required this.model,
    required this.equipmentType,
    required this.tippingLoad
    });

  describe() {
    return 'The Loader ${this.model} is a ${this.equipmentType} and has a tipping load of ${this.tippingLoad.toString()}.';
  }
}
