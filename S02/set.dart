void main() {
  /* 
    A set in Dart is an unordered collection of unique items.
      - same as a Map{} that uses curly braces,but for set the data are not in key-value pairs, but values only
      -only stores unique items, bawal magkapareparehas 
      ex:
        {'one','two','one'} --> {'one','two'}
    Syntax:
    Set<dataType> variableName = {values}

    Sets can also use List Methiods
  */

  Set<String> subcontractors = {'Sonderhoff', 'Stahlschmidt'};
  subcontractors.add('Schweisstechnik');
  subcontractors.add('KreateL');
  subcontractors.add('Kunstoffe');

  print(subcontractors);
  subcontractors.remove('Sonderhoff');

  print(subcontractors);
}
