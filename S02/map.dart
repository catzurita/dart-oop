void main() {
  /* 
    Map()
    - syntax: Map<keyDataType, valueDataType> = {values}
   */
  Map<String, String> address = {
    'specifics': '221B Baker Street',
    'district': 'Marylebone',
    'city': 'London',
    'country': 'United Kingdom'
  };
  // Adding a value in a map
  address['region'] = 'Europe';
  print(address);
}
