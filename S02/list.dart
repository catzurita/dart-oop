void main() {
  // Syntax:
  // List<dataType> variableName = [values]
  List<int> discountRanges = [20, 40, 60, 80];
  List<String> names = ['John', 'Jane', 'Tom'];
  const List<String> maritalStatus = [
    'single',
    'married',
    'divorce',
    'widowed'
  ];

  print(discountRanges);
  print(names);
  // Accessing lists
  print(discountRanges[0]);
  print(names[2]);
  // length - shows the number of data in a lists
  print(discountRanges.length);
  print(names.length);
  // Changing the values of a list
  names[0] = 'Jonathan';
  print(names);
  /* 
  If we want to make the values unchangeable we put const 
  ex: 
  const List<String> names = ['John', 'Jane', 'Tom'];
  or 
  List<String> names = const ['John', 'Jane', 'Tom'];
   */

  // Adding data or values to a list
  /* 
  .add() - adds the variable at the end of the list
  .insert(index, value) - adds a variable to the specified index number

   */
  names.add('Kobe');
  names.insert(0, 'Lebron'); //Adds a value in front of the list
  print(names);

  print(names.isEmpty);
  // Method that checks if the list is empty or not, returns a boolean value1
  print(names.isNotEmpty);
  print(names.first);
  print(names.last);
  print(names.reversed);

  /* 
  sort() - is a method that modifies the list itself
  kaya yung mismong list na sinort, yun na din derecho yung magiging list ng sorted list
  - by default, it sort alphabetically
   */

  names.sort();
  print(names);

  //remove() - removes a value in a set
}
