void main() {
  Set<String> pcParts = {
    'Ryzen 5 3600',
    'Ryzen 3 3200G',
    'Ryzen 7 5800X',
    'Ryzen 9 5950X'
  };

  Map<String, dynamic> person = {
    'First Name': 'John',
    'Last Name': 'Smith',
    'Age': 34
  };

  List<String> countries = [
    'Philippines',
    'Thailand',
    'Singapore',
    'Canada',
    'Japan',
    'United Kingdom',
    'Canada'
  ];

  print(pcParts.length);
  print(person.length);
  print(countries.length);

  print(pcParts.elementAt(2));
}
