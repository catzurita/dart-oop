void main() {
  Person person = new Person(firstName: 'John', lastName: 'Smith');

  Employee employee =
      new Employee(firstName: 'John', lastName: 'Smith', employeeId: 'Id-001');

  print(person.getFullName());
  print(employee.getFullName());
  print(employee.employeeId);
}

class Person {
  String firstName;
  String lastName;

  Person({required this.firstName, required this.lastName});

  String getFullName() {
    return '${this.firstName} ${this.lastName}';
  }
}

class Employee extends Person {
  // Creating a super constructor
  /* 
   Class({input values}): super(keyValueFromParent: inputValueForParent)

   Class({input values}): super(parameters)

   the employee inherits the methods and parameters from person

   inheritance allows us to write less code 
   */
  String employeeId;
  Employee({required firstName, required lastName, required this.employeeId})
      : super(firstName: firstName, lastName: lastName);

  String getEmployeeId() {
    return this.employeeId;
  }
}
