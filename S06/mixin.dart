void main() {
  Equipment equipment = new Equipment();
  equipment.name = 'Equipment-001';

  Loader loader = new Loader();
  loader.name = 'Loader-001';

  Car car = new Car();
  car.name = 'Crane-001';

  print(equipment.name);
  print(loader.name);
  print(loader.getCategory());

  print(car.name);
  print(car.getCategory());
  car.moveForward(20);
}

class Equipment {
  String? name;
}

class Loader extends Equipment with Movement {
  String getCategory() {
    return '${this.name} is a loader.';
  }
}

class Car with Movement {
  String? name;
  String getCategory() {
    return '${this.name} is a car';
  }
}

mixin Movement {
  // It can also be used for variables

  num? acceleration;
  void moveForward(num acceleration) {
    this.acceleration = acceleration;
    print('The vehicle is moving forward at ${acceleration}');
  }

  void moveBackward() {
    print('The vehicle is moving Backward');
  }
}
/* 
Mixins: 
  - Allows heirarchichal without extension
  - if 2 or more unrelated classes requires the same method or properties

  gingamit sya for things that are not related but have the same behavior

  with - includes the mixin class to be inherited by the class 

  mixin keyword - declares a class to be a mixin

  Ginagamit lang mixin as long as a group of unrelated objects have the same movements or variables
 */