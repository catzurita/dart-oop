import './freezed_models/user.dart';

void main(){
    User userA = User(id: 1, email: 'john@gmail.com');
    User userB = User(id: 1, email: 'john@gmail.com');

    print(userA.hashCode);
    print(userB.hashCode);
    print(userA == userB);

    // For this, kita na agad yung error, hindi pa nararun yung code.Dahil ginawan na sya agad ng error or stopper sa freezed 
    // This is an example of object immutability

    //Instead of changing  the object's property, 
    // The object itself will be changed. 
    /* 
    Instead of this: 

    print(userA.email);
    userA.email = 'john@hotmail.com';
    print(userA.email); 
    */

    // We use

    print(userA.email);
    userA = userA.copyWith(email: 'john@hotmail.com');
    print(userA.email);


    // using the json serializable 
    var response = {'id': 3, 'email': 'doe@gmail.com'};

    /* 
    Building a User with json variables without the json serializable 

    User userC = User(
        id: response['id'] as int, 
        email: response['email'] as String 
    );
     */
    
    // With json serializable, nakabase naman sa variables ng freezed class natin to
    // valit naman yung nasa taas, kaso mas convenient gamitin to since mas short sya 
    User userC = User.fromJson(response);
    print(userC);
    print(userC.toJson());

}
