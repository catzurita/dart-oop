import 'package:freezed_annotation/freezed_annotation.dart';

part 'note.freezed.dart';
part 'note.g.dart';

@freezed
class Note with _$Note{
    const factory Note({
        required int id,
        required int userId,
        required String title,
        required String description
    }) = _Note;

    // This line is a part of the implementation of the package json serializable 
    factory Note.fromJson(Map<String, dynamic> json) => _$NoteFromJson(json);
}
