import 'package:freezed_annotation/freezed_annotation.dart';

// NOTE BY USING THE FREEZE PACKAGE IN COMPARING THE CHARACTERISTICS OF OBJECTS

/* 
part says that you have a dart file wherein the rest of its content is in user.freezed.dart
 - may part ng code na nakalagay sa user.freezd.dart, parang pinaghiwahiwalay mo yung code into separate parts,

 - si user.dart, yung ibang file or part ng code nya is nakalagay kay user.freezed.dart
 = the rest of the contents for user.dart can be found in user.freezed.dart using the 'part' keyword
 */
part 'user.freezed.dart';
// This line is a part of the implementation of the package json serializable 
part 'user.g.dart';


/* 
    with keyword -mag generate sya ng mixin called _$User sa loob ng user.freezed.dartibig sabihen, si class User, is nadagdaggan with this _$User class, 

    factory - creation of an object based on a class 
        - Use the factory keyword when implementing a constructor that doesn’t always create a new instance of its class. 

    @freezed annotation - tells the build_runner to create a freezed class named _$User in user.freezed.dart

    after calling dart run build_runner build, - mag autogenerate to ng user.freezed.dart na file, 
*/
@freezed
class User with _$User{
    const factory User({
        required int id,
        required String email,
        String? accessToken
    }) = _User;

    // This line is a part of the implementation of the package json serializable 
    factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}