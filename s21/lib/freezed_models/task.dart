import 'package:freezed_annotation/freezed_annotation.dart';

part 'task.freezed.dart';
part 'task.g.dart';

@freezed
class Task with _$Task{
    const factory Task({
        required int id,
        required int userId,
        required String description,
        String? imageLocation,
        required int isDone
    }) = _Task;

    // This line is a part of the implementation of the package json serializable 
    factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);
}

