import './models/user.dart';

void main(){
    User userA = User(id: 1, email: 'john@gmail.com');
    User userB = User(id: 1, email: 'john@gmail.com');

    // print(userA.hashCode);
    // print(userB.hashCode);
    // print(userA == userB);

    // An example of comparing objects, by their characteristics excluding the unique identifiers
    // bool isIdSame = userA.id == userB.id;
    // bool isEmailSame = userA.email == userB.email;
    // bool areObjectSame = isIdSame && isEmailSame;

    // print(areObjectSame);.

    print(userA.email);
    userA.email = 'john@hotmail.com';
    print(userA.email);
    //These line of codes, will result to an error, since final na din ang type,
    //pero mararun pa din ang code, mageerror lang sya once na run mo na yung code  
}

// Dart does not check the values of the object
// but also its unique identifier(through hashCodes)