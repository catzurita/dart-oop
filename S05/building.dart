class Building {
  String? _name;
  int floors;
  String address;

// this._name - is a positional required argument
  Building(this._name, {required this.floors, required this.address}) {
    print('A building the object has been created');
  }

  /* 
     Accesors, setters-getters
     allows the indirect access of variables

     Adding '_' infront of a parameter name, makes the parameter private
      - this means it cannot be accessed by codes on other files, pero naaccess sya ng codes from the same file 

      - maacces lang yung private parameter ng isang class sa ibang file by using accessors(setters-getters)
   */
  // Importante yung keyword na get at set, built in na siya sa dart na method
  String? get Name {
    print('The building\'s name has been retrieved');
    return this._name;
  }

  void set Name(String? name) {
    this._name = name;
    print('Building\'s name has been changed');
  }
  /* 
  pag kinall ang setter 
    building.Name = 'newBuilding Name'
   */

  //Method creation in a class
  Map<String, dynamic> getProperties() {
    return {
      'name': this._name,
      'floors': this.floors.toString(),
      'address': this.address
    };
  }

  // pwede din hindi na mag lagay ng return type for the method, automatic sya magiging dynamic
  /*
  pwede to:

   getProperties() {
    return {
      'name': this.name,
      'floors': this.floors.toString(),
      'address': this.address
    };
  }
   */
}
