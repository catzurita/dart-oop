import './building.dart';

void main() {
  Building building = new Building('Caswynn',
      floors: 8,
      address:
          '134 Timog avenue, Brgy. Sacred Heart, Quezon City, Metro Manila');
  /* 
    - in making an instance, it can be defined as the class, but for dart,
    pwedeng var lang ang ideclare, at kahit di na gumamit ng new
*/
  // print(building._name);
  print(building.Name);
  building.Name = 'GEMPC';
  print(building.Name);
  print(building.floors);
  print(building.address);
  print(building.getProperties());
}



/* 
Class declaration syntax:
    class ClassName{
        <fields>
        <constructors>
        <methods>
        <getters-or-setters>
    }

to access an instance of a class, since it's an object, dot notation lang ang gamit
*/
