void main() {
  Building building = new Building(
      name: 'Caswynn',
      floors: 8,
      address:
          '134 Timog avenue, Brgy. Sacred Heart, Quezon City, Metro Manila');
  /* 
    - in making an instance, it can be defined as the class, but for dart,
    pwedeng var lang ang ideclare, at kahit di na gumamit ng new
*/

  print(building.name);
  print(building.floors);
  print(building.address);
}

class Building {
  /* 
        late means the value of the variable will be given later
        - possible solution if variables is not initialized on compile time
     */
  String? name;
  int floors;
  String address;
//   List<String> rooms;

  /* 
    For Dart, making a constructor is by using the same name as the class, it can the receive parameters that the class can use
     */
  // Ex: of constructor creation using positional arguments
//   Building(this.name, this.floors, this.address) {
//     print('A building the object has been created');
//   }

  // Ex: of construtor cration using optional named parameters

  Building({this.name, required this.floors, required this.address}) {
    print('A building the object has been created');
  }

  /* 
    Nilalagay lang yung required if talagang required na may value ang isang variable, sa instance making, if walang inaassign di sya mageerror

  since parameters naman, naka based sa pag kasunod sunod ng pagdeclare yung pag assign ng values sa loob ng class
   - direct assignment sya
  Shortcut for class creation is the same as this
    Building(String name, int floors, String address) {
    this.name = name;
    this.floors = floors;
    this.address = address;
  }
  
   */
}

/* 
Class declaration syntax:
    class ClassName{
        <fields>
        <constructors>
        <methods>
        <getters-or-setters>
    }

to access an instance of a class, since it's an object, dot notation lang ang gamit
*/
