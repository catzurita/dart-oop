/* 
Using a function, get the total price of prices [45, 34.2, 176.9, 32.2] and optionally get its discounted price.

Use a loop to get the total price.

288.3 (no discount)
230.64 (20% discount)
172.98 (40% discount)
115.32 (60% discount)
57.66 (80% discount)
 */

void main() {
  List<num> prices = [45, 34.2, 176.9, 32.2];

  // for no discount
  print(getSum(prices));
  // for 20 percent discount
  print(getDiscount(20)(getSum(prices)));
  // for 40 percent discount
  print(getDiscount(40)(getSum(prices)));
  // for 60 percent discount
  print(getDiscount(60)(getSum(prices)));
  // for 80 percent discount
  print(getDiscount(80)(getSum(prices)));
}

num getSum(List<num> numList, {num count = 0}) {
  numList.forEach((each) => count += each);
  return count;
}
/* 
One-line alternative to get the total
num getSum(List<num> numList) =>numList.reduce((numA,numB) => numA+numB) 
 */

Function getDiscount(num percentage) {
  return (num amount) {
    return amount * (100 - percentage) / 100;
  };
}
