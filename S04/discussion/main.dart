// The main function is the entry point of Dart application

void main() {
  print(getCompanyName());
  print(getYearEstablishment());
  print(hasOnlineClasse());
  print(getCoordinates());
  print(combineAddress(
      '134 Timog Avenue', 'Brgy. Sacred heart', 'Quezon City', 'Metro Manila'));
  print(combineName('John', 'Smith'));
  print(combineName('John', 'Smith', isLastNameFirst: true));
  print(combineName('John', 'Smith', isLastNameFirst: false));

  /* 
        Anonymous functions - are used when that function is only used once
    */
  List<String> persons = ['John Doe', 'Jane Doe'];
  List<String> students = ['Nichole Rush', 'James Holden'];

/* 
    Functions as objects and used as an argument
     - si forEach -> tinuro nya yung List as an input for the printName function,

     printName(value) is function execution/call/invocation
      - tinuturo lang natin yung function na printName sa loob ng forEach

      printName is reference to the given function
 */
  persons.forEach(printName);
  students.forEach(printName);
  print(isUnderAge(18));
}
/* *************************************** */

// Example as printName being referenced
void printName(String name) {
  print(name);
}

/*  
Optional named parameters - therse are parameters added after the required ones.
    - these parameters are added inside a curly bracket
    - if no value is given, a default value can be assigned

*/
// Example of Optional named parameters
String combineName(String firstName, String lastName,
    {bool isLastNameFirst = false}) {
  if (isLastNameFirst) {
    return '$lastName $firstName';
  } else {
    return '$firstName $lastName';
  }
}

String combineAddress(
    String specifics, String barangay, String city, String province) {
  return '$specifics, $barangay, $city, $province';
}

String getCompanyName() {
  return 'FFUF';
}

int getYearEstablishment() {
  return 2021;
}

bool hasOnlineClasse() {
  return true;
}

/* 
NOTE: For dart, writing the data type of a function parameter is not required
    - The reason is related to code readability and concisenes 
    - if no Data type is specified to a parameter, the default data type is Dynamic
    - convenient na walang data type, pero it's more effective if meron

()=> - lambda or arrow function
    - this is a shortcut function for returning values from simple operations
    - The Syntax of a lambda function is:
        <returnType> functionName(parameters) => expression;

isUnderAge function into a lambda function

bool isUnderAge(int age) {
  return (age < 18) ? true : false;
}
*/
bool isUnderAge(age) => (age < 18) ? true : false;

Map<String, double> getCoordinates() {
  return {'latitude': 16.32702, 'longitude': 121.04334};
}
/* 
The following syntax is followed when creating a function

<return-type> <functionName>(param-data-type parameterA, param-data-type parameterB){
  return 'return-type';
}

arguments are the values being passed by the function
parameters are the values being received by the function
 */