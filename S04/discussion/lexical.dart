void main() {
  Function discountBy25 = getDiscount(25);
  Function discountBy50 = getDiscount(50);
  print(discountBy25(1000));
  print(discountBy50(1000));
  // alternative syntax if we want to use getDiscount directly
  print(getDiscount(25)(1000));
  print(getDiscount(50)(1000));
}

/* 
   This function returns a function:
   with this, data discountBy25 receives a function which is the return value of getDiscount
   - We can now use discountBy25 as a function wherein it will take an argument which is the amount in the return value of the function getDiscount

  - when the getDiscount is used and the function is returned
  - the value of 'percentage' parameter is retained inside  the anonymous function, and that is called 'Lexical Scope'

  - The discountBy25 is then considered as a 'Closure'
  - the closure has access to variable in its lexical scope
 */
Function getDiscount(num percentage) {
  return (num amount) {
    return amount * percentage / 100;
  };
}
