import 'package:flutter/material.dart';

// Example of making a customize widget
class AnswerButton extends StatelessWidget {
  final String answers;
  final Function nextQuestion;
  // Constructor for AnswerButton
  AnswerButton({
    required this.answers,
    required this.nextQuestion
  });

  @override
  Widget build(BuildContext context) {
    return RadioListTile<String>(
            title: Text(answers),
            value: answers,
            groupValue: null,
            onChanged: (value){
              SnackBar snackBar = SnackBar(
                content: Text('You have selected $value'),
                duration: Duration(milliseconds: 2000),
                );
               //ginawa to para pwede na din magawa yung function pag pinindot yung mga elements ng RadioList
               ScaffoldMessenger.of(context).showSnackBar(snackBar);
               nextQuestion(value);
            },
          );
  }
}

/* 

Nilagyan na ng value yung nextQuestion kasi ang aim is kelangan na natin mag return ng value pag may sinelect na option,

Maibabalik na yung value once pumili ng option, 
the vallue will be submitted back sa main.dart

 */