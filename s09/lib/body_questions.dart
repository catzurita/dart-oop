import 'package:flutter/material.dart';
import './answer_button.dart';

class BodyQuestions extends StatelessWidget {
    final questions;
    final int questionIdx;
    final Function nextQuestion;

    BodyQuestions({
        required this.questions,
        required this.questionIdx,
        required this.nextQuestion
    });

    @override
    Widget build(BuildContext context) {

        Text questionText = Text(
            questions[questionIdx]['question'].toString(),
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold
            )
        );

        List<String> options = questions[questionIdx]['options'] as List<String>;

        var answerOptions = options.map((String option){
        return AnswerButton(answers: option, nextQuestion: nextQuestion);
        });

        return Container(
            width: double.infinity, 
            padding: EdgeInsets.all(16),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                    questionText,
                    ...answerOptions,
                    Container(
                        margin: EdgeInsets.only(top: 20),
                        width: double.infinity,
                        child: ElevatedButton(
                                child: Text('Skip Question'),
                                onPressed:() => nextQuestion(null),
                                 // the next question method is used here as a variable, it's a pointer for the method of nextQuestion, since si function ay need ng argument and need lang natin gamitin yung function, naglagay lang ng value na null
                                ),
                    )
                ]
            ),
        );
        
  }
}