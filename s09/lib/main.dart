import 'package:flutter/material.dart';
import './body_questions.dart';
import './body_answers.dart';

// Import statement needed, this deals with the widgets within flutter
void main() {
  runApp(App());

}

class App extends StatefulWidget {
  
  @override
  AppState createState() => AppState();
  // way to create a astate
}

class AppState extends State<App>{
  
    int questionIdx = 0;
    bool showAnswers = false;
    final questions = [
        {
            'question': 'What is the nature of your business needs?',
            'options' : ['Time Tracking', 'Aset Management', 'Issue Tracking']
        },
        {
            'question': 'What is the expected size of the user base?',
            'options' : ['Less tan 1,000', 'Less than 10,000', 'More than 10,000']
        },
        {
            'question': 'In which region would the majority of the user be?',
            'options' : ['Asia', 'Europe', 'Americas', 'Africa', 'Middle East']
        },
        {
            'question': 'What is the expected project duration?',
            'options' : ['Less than 3 months', '3-6 months', '6-9 months', '9-12 months', 'More than 12 months']
        },
    ];

    var answers = [];

    void nextQuestion(String? answer){
        answers.add({
            'question': questions[questionIdx]['question'],
            'answer': (answer == null) ? '' : answer
        });

        print(answers);

        if(questionIdx < questions.length -1){
            setState(() => questionIdx++);
        } else {
            setState(() => showAnswers = true);
        }
    
    }
    
    @override
    Widget build(BuildContext context) {
        var bodyQuestions = BodyQuestions(
            questions: questions, 
            questionIdx: questionIdx, 
            nextQuestion: nextQuestion,
            );
        var bodyAnswers = BodyAnswers(answers: answers);

        Scaffold homepage = Scaffold(
            appBar: AppBar(
                title: Text('Homepage'),
            ),
            body: (showAnswers) ? bodyAnswers : bodyQuestions  
        );

        return MaterialApp(
            home: homepage
        );
    }
  /*  
  MaterialApp widget - came from the material.dart package
  parang gagawa din ng ainstance of  a MaterialApp
    return new MaterialApp() pero since dart, okay lang kahit wala ng new 

  Text widget - handles the rendering of the text input
  */
}

/* 
The build method in classes or widgets, is the responsible in generating or rendering the widgets
 */

// The invisible widgetsa are the container and scaffold
    // The visible widgets are AppBar and Text

    /* 
    - To specify margin or padding spacing, we can use EdgeInsets
    - The values for spacing are multiples of 8(eg. 16,24,32)
    - To add spacing on all sides, use EdgeInsets.all()
    - to add spacing on certain sides, use EdgeInsets.only(direction:value)

    double.infinity - is used to tell the size that it will take up all the available size ex:
    width: double.infinity - sagad yung width na itetake nya sa screen
        almost same sya sa, double.maxFinite
        - double.infinity - refers to the infinity itself
        - double.maxFinite - refers to the maximum available fvalue of a double

    decoration: BoxDecoration(color: Colors.green) can be used to put colors on a container

    In a column widget: 
      - The MainAxisAlignment is a vertical (from top to bottom)
      - THe CrossAxisAlignment is used the horizontal postion or alignment of widget in a column 
      
    In the context of the column we can consider the column as vertical, and its cross axis as horizontal
     */
    

    /* 
     The Scaffold widget provides basic design layout structure
      - it can be given UI elements such as an appbar
     */

    /* 
    The state is lifted up
        App handles the state -> the bodyQuestion is a stateless widget but uses the state of the App file, so the state is passed down to bodyQuestion, nagagamit pa din ni bodyQuestion yung state kahit nasa ibang file
    The App.questionIdx
        -> being used by BodyQuestions.questionIdx
    */