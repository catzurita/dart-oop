void main() {
  // Assignment Operators - assigns a value to a variable
  int x = 1397;
  int y = 7831;

  // Arithmetic Operators
  // Kahit type int yung x and y, num accepts both int and double
  num sum = x + y;
  num difference = x - y;
  num product = x * y;
  num quotient = x / y;
  num remainder = 4 % 3; //modulo, returns the remainder

  print(remainder);

  // Relational Operators
  bool isGreaterThan = x > y;
  bool isLessThan = x < y;

  bool isGTorEqual = x >= y;
  bool isLTorEqual = x <= y;

  bool isEqual = x == y;
  bool isNotEqual = x != y;

  // Logical Operators
  bool isLegalAge = true;
  bool isRegistered = false;

  bool areAllRequirementsMet = isLegalAge && isRegistered;
  bool areSomeRequirementsMet = isLegalAge || isRegistered;
  bool isNotRegistered = !isRegistered;

  // Increment and decrement operators
  print(x++);
  print(y--);
}
