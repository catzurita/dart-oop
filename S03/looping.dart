void main() {
  // whileLoop();
  // doWhile();
  // forLoop();
  // forInLoop();
  modifiedForLoop();
}

void whileLoop() {
  int count = 5;

  while (count != 0) {
    print(count);
    count--;
  }
}

void doWhile() {
  int count = 20;

  do {
    print(count);
    count--;
  } while (count > 0);
}

void forLoop() {
  //for Loop, initializer, condition, action
  for (int count = 0; count <= 20; count++) {
    print(count);
  }
}

void forInLoop() {
  List<Map<String, String>> persons = [
    {'name': 'Brandon'},
    {'name': 'John'},
    {'name': 'Arthur'},
  ];

// person, is to define a single data in persons, parang forEach sya na nagdedeclare ng tawag sa per data sa list, var is used since para kahit anong data type yung madedetermine sa list
  for (var person in persons) {
    print(person);
    // Printing the value only
    print(person['name']);
  }
}

void modifiedForLoop() {
  for (int count = 0; count <= 20; count++) {
    if (count % 2 == 0) {
      continue;
      //continue is called a branching statement that goes to its next iteration, back to top
    } else {
      print(count);
    }

    if (count > 10) {
      break;
      // means exit of the loop
    }
  }
}
